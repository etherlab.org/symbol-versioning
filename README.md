# Symbol Versioning tool

This is a tiny python script which helps us manage our C/C++ libraries.
It needs the `yaml` module to be installed.

We enrolled symbol versioning primarily to help the RPM dependency
tracker to learn about additions in our libraries.
If a new feature is added to a library (e.g. new functions/methods),
dependent applications using this feature needs to have this new library version installed.
The RPM dependency tracker uses the `SONAME` to resolve the dependency,
so this can be used to force the automatic upgrade of the installed libraries.
However, changing the `SONAME` (or the `SOVERSION`) will break existing applications which
do not need the new library feature.

Luckily, it also honors the symbol versions in a library.
So, in each library, there is a symbol.map.yaml file. See below for details.
This file is used to generate a linker script during the build process,
to check whether all public symbols have a version tag and to generate a
[symbols file](https://manpages.debian.org/unstable/dpkg-dev/deb-src-symbols.5.en.html)
for debian packages.


## TLDR: Rules of thumb

 - Make sure all changes of the public API are
   [backwards compatible](https://community.kde.org/Policies/Binary_Compatibility_Issues_With_C%2B%2B).
   If in doubt, ask.
 - New functions/methods get a new version tag and the minor version is increased.
   So, do not add new content to an exsisting group in the yaml file.
 - Make sure that the minor version in the version tag always correspond with the
   project version where a feature has been implemented.
   Otherwise, the debian dependency tracker does not work.
 - Never ever remove contents from non-private sections in the yaml file.
   Exception: moving symbols from `all` to an arch-specific group.
 - If you have to break the public API, change the `SOVERSION`.
 - If exposed internals are changed (private sections),
   modify their version tag to reflect these changes. I.e., increase patch version.
 - All functions which have a `size_t` as argument are architecture-dependent.
 - Things which do not directly belong to the library (STL,
   vtable/typeinfo of other librares) go to `ignored_symbols`.
 - Weak functions are ignored per default, but you can mark them as mandatory.
   This can be useful if templates are instantiated explicitly.

### HOWTO: Adding new functions to a library

 - Implement new functions, increase minor version and set patch version to 0.
 - Call CMake with `-DUSE_SYMBOL_VERSIONING=1` and rebuild.
 - The script will complain about unversioned symbols.
 - Add a new group in the yaml file with the new minor version.
 - Add these unversioned symbols into the new group.
 - Also verify whether the new symbols are arch-dependent,
   i.e. by compiling on other targets with OBS.

### Notes about our versioning convention

In general, our projects have a version scheme which consists of three components, `major.minor.patch`.
The major version number is always included in the library name,
e.g. `PdCom5`.
It is increased when very big changes occur.

If a new feature has been implemented, the minor version is bumped.
New features bring new symbols into the library,
so they are grouped into a new group with the same minor version.

Changes in the library which do not affect the public API,
for instance a small fix in the internals,
will cause the patch version to increase.

Of couse, if the minor is increased, the patch version is set to 0 again.
Same applies for minor if major is bumped.

The `SOVERSION` however is a monotonic increased number, starting at 1,
which is changed when the public API/ABI is changed in an incompatible way.
We really want to avoid that, but if we have to,
we'll have to let our users know.

To allow parallel installation of multiple versions of the library,
the full file name of a library is
`lib<Libname><Major>.so.<SOVERSION>.<Minor>.<Patch>`.
The `SONAME` is `lib<Libname><Major>.so.<SOVERSION>`.

## Usage

At minimum, you'll have to supply a symbol.map.yaml file with `-i`
and one of the following commands.
An output file can be specified by `-o`, default is stdout.

### GenerateLinkerVersionScript

Generate a version script for the linker (`-version-script` option for GNU ld).
Optionally, an architecture can be specified (`--arch`),
which is one of `generic{32,64}` or `generic{32,64}-lto`.
The `-lto` flavor was introduced because some distributions build
their packages with `-flto` (Link Time Optimization),
which has an influence on the symbol table.
For further differentation, `x86` and `arm` are also available.

### CheckGeneratedLibraries

Reads all public symbols from a library (`--library` option) with `readelf` and
verifies that all symbols are included in the symbol.map.yaml file.
Also makes sure that no symbol has been removed from the library,
as this would be backwards incompatible.

Missing symbols and unversioned (symbols not mentioned in the symbol.map.yaml file)
are listed in the output and the return code is non-zero.

### GenerateDebianSymbolsFile

Generate a
[symbols file](https://manpages.debian.org/unstable/dpkg-dev/deb-src-symbols.5.en.html).
This is only needed during the build of debian packages, after the library has been built.
Usually this is done in `override_dh_auto_install` in `debian/rules`.
The package name in the symbols file is derived from the `SONAME`, according to the
[debian convention](https://www.debian.org/doc/debian-policy/ch-sharedlibs.html#run-time-shared-libraries).

## File format

The yaml file has two main objects: `symbols` and `ignored_symbols`.
The latter one is a list with all symbols which should be ignored,
typically template instantiations of the C++ STL
or the library constructors/destructors (`_init` and `_fini`).

`symbols` contains dictionaries, whose keys are used as version tag in the linker script.
The keys usually are the capitalized library name plus major and minor version,
for instance `PDCOM_5.1`.
The value is also a dictionary of lists.
The keys of these dictionaries are used to group the symbols,
according to the architecture (`generic{32,64}`, `generic{32,64}-lto` or the special value `all`).
The symbols in the `all` group are always used.
Furthermore, the key-value pair `is_private`, `{true,false}` can be specified to mark
internals which have to be public, too.
The version tag for internals should have three digits.
If these exposed internals are changed, increase the patch version
to make sure all dependent libraries are recompiled.
In short, internals are expected to be changed in a backwards-incompatible way,
and we should not lie to the RPM dependency tracker.

Example from [PdCom5](https://gitlab.com/etherlab.org/pdcom/-/blob/356230f6921dbbe7ceba187fef6537a83a540511/src/pdcom.map.yaml):
```yaml
symbols:
  # Internals, only to be used by libpdcom5-sasl.so and libpdcom5-gnutls.so.
  # Adding, changing and removing symbols here is fine. But please keep in mind
  # to update the version number in the tag accordingly, so that the
  # dependency trackers have a chance to lean about these changes.
  PDCOM_PRIVATE_5.2.0:
    is_private: true
    all:
      # OBJECT GLOBAL typeinfo name for PdCom::impl::Process
      - _ZTSN5PdCom4impl7ProcessE
      # FUNC GLOBAL PdCom::Process::Process(std::shared_ptr<PdCom::impl::Process>)
      - _ZN5PdCom7ProcessC1ESt10shared_ptrINS_4impl7ProcessEE
      # FUNC GLOBAL PdCom::impl::IOLayer::~IOLayer()
      - _ZN5PdCom4impl7IOLayerD0Ev
  PDCOM_5.1:
    all:
      # FUNC GLOBAL PdCom::PosixProcess::~PosixProcess()
      - _ZN5PdCom12PosixProcessD2Ev
      # FUNC GLOBAL PdCom::Process::name[abi:cxx11]() const
      - _ZNK5PdCom7Process4nameB5cxx11Ev
    generic32:
      # FUNC GLOBAL PdCom::PosixProcess::posixWriteDirect(char const*, unsigned int)
      - _ZN5PdCom12PosixProcess16posixWriteDirectEPKcj
    generic64:
      # FUNC GLOBAL PdCom::PosixProcess::posixWriteDirect(char const*, unsigned long)
      - _ZN5PdCom12PosixProcess16posixWriteDirectEPKcm

ignored_symbols:
  # OBJECT WEAK vtable for std::_Sp_counted_ptr_inplace<PdCom::impl::Process, std::allocator<void>, (__gnu_cxx::_Lock_policy)2>
  - _ZTVSt23_Sp_counted_ptr_inplaceIN5PdCom4impl7ProcessESaIvELN9__gnu_cxx12_Lock_policyE2EE
  # OBJECT UNIQUE std::_Sp_make_shared_tag::_S_ti()::__tag
  - _ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag
```

# Copyright (C) 2023 Bjarne von Horn (vh at igh dot de).
#
# This file is part of the QtPdCom library.
#
# The QtPdCom library is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# The QtPdCom library is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
# License for more .
#
# You should have received a copy of the GNU Lesser General Public License
# along with the QtPdCom library. If not, see <http://www.gnu.org/licenses/>.


import argparse
from collections import OrderedDict
import re
import sys
import subprocess
import typing
import yaml

def ordered_load(stream, Loader=yaml.SafeLoader, object_pairs_hook=OrderedDict):
    class OrderedLoader(Loader):
        pass
    def construct_mapping(loader, node):
        loader.flatten_mapping(node)
        return object_pairs_hook(loader.construct_pairs(node))
    OrderedLoader.add_constructor(
        yaml.resolver.BaseResolver.DEFAULT_MAPPING_TAG,
        construct_mapping)
    return yaml.load(stream, OrderedLoader)


def cxxfilt(s: str):
    proc = subprocess.run(
        ["c++filt", s],
        check=True,
        stdout=subprocess.PIPE,
        encoding="ascii",
    )
    return proc.stdout.splitlines()[0]


class Symbol:
    def __init__(self, name, type, bind, **kwargs):
        self._name = name
        self._type = type
        self._bind = bind
        self._symbol_version = kwargs.get("version", "")
        if self._symbol_version is None:
            self._symbol_version = ""

    def is_weak(self):
        return self._bind == "WEAK"

    def is_function(self):
        return self._type == "FUNC"

    def is_object(self):
        return self._type == "OBJECT"

    def mangled_name(self):
        return self._name

    def unmangled_name(self):
        return cxxfilt(self._name)

    def symbol_version(self):
        return self._symbol_version

    def symbol_version_or(self, default_value):
        if len(self._symbol_version) == 0:
            return default_value
        self._symbol_version


readelf_symbol_re = re.compile(
    r"""^\s+\d+:\s+               # symbol number and colon
        [0-9a-f]+\s+              # value/address of symbol
        \d+\s+                    # size of symbol
        (?P<type>[A-Z]+)\s+       # type of symbol (FUNC, OBJECT, ...)
        (?P<bind>[A-Z]+)\s+       # bind type (GLOBAL, WEAK, UNIQUE)
        (?P<visibility>[A-Z]+)\s+ # visibility (GLOBAL, LOCAL)
        \d+\s+(?P<name>\w+)       # symbol name
        # optional symbol version, separated by @
        # second @ means default version
        (@{1,2}(?P<version>[\w.]+))?\s*$""",
    re.ASCII | re.VERBOSE
)

readelf_soname_re = re.compile(
    r"""\s*0x[a-f0-9]+\s+    # Tag
    \(SONAME\)\s+         # SONAME Type
    [\s\w]+:\s+           # Entry Name
    \[([-\d\w.]+)\]        # SONAME in square brackets
    """,
    re.ASCII | re.VERBOSE
)


class SharedLibrary:
    def __init__(self):
        self._soname = ""
        self._symbols = []

    def readElfFile(self, f: str):
        self._read_symbols(f)
        self._read_soname(f)

    def _read_symbols(self, f):
        self._symbols = []
        args = ["readelf", "--wide", "--dyn-syms", f]
        readelf = subprocess.run(
            args,
            check=True,
            stdout=subprocess.PIPE,
            encoding="ascii",
        )
        for line in readelf.stdout.splitlines():
            match = readelf_symbol_re.match(line)
            if match is None or match.group("visibility") != "DEFAULT":
                continue
            if match.group("type") == "NOTYPE":
                continue
            self._symbols.append(Symbol(**match.groupdict()))

    def _read_soname(self, f):
        self._soname = ""
        args = ["readelf", "--dynamic", f]
        readelf = subprocess.run(
            args,
            check=True,
            stdout=subprocess.PIPE,
            encoding="ascii",
        )
        for line in readelf.stdout.splitlines():
            match = readelf_soname_re.match(line)
            if match is not None:
                self._soname = match.group(1)
                break

    def symbols(self) -> typing.Iterable[Symbol]:
        return self._symbols[:]

    def soname(self):
        return self._soname

    def getWeakFunctions(self) -> typing.Iterable[Symbol]:
        return [s for s in self._symbols if s.is_weak() and s.is_function()]

    def debianPackageName(self):
        # see https://www.debian.org/doc/debian-policy/ch-sharedlibs.html#run-time-shared-libraries
        # for debian packaging name conventions.
        # in short: lowercase(librarynameSOVERSION). If libraryname ends with number, separate by dash.
        if self._soname.endswith(".so"):
            name = self._soname[:-3]
            return name.replace("_", "-").lower()
        name, soversion = self._soname.split(".so.")
        name = name.replace("_", "-")
        if name[-1].isnumeric() and len(soversion):
            return (name + "-" + soversion).lower()
        else:
            return (name + soversion).lower()


class SymbolMap:
    def load(self, f):
        self._content = ordered_load(f, yaml.SafeLoader)

    def generateLinkerVersionScript(self, f: typing.IO, arch: str):
        last_version_tag = None
        for version_tag, _symbols in self._content["symbols"].items():
            f.write(version_tag + " {\n")
            def symbols(key):
                if key in _symbols and _symbols[key] is not None:
                    return _symbols[key]
                return []
            for symbol in symbols("all"):
                f.write("    %s;\n" % symbol)
            for symbol in symbols(arch):
                f.write("    %s;\n" % symbol)
            if last_version_tag is not None:
                f.write("} %s;\n" % last_version_tag)
            else:
                f.write("};\n")
            last_version_tag = version_tag

    def checkGeneratedLibraries(self, arch: str, _sofile: str, outfile: typing.IO):
        ret = True
        def get_iter(dict, key):
            if key in dict and dict[key] is not None:
                return dict[key]
            return []
        library = SharedLibrary()
        library.readElfFile(_sofile)
        symbols_not_in_yaml = []
        mandatory_symbols = {}
        ignored_symbols = get_iter(self._content, "ignored_symbols")
        for version_tag, symbol_obj in self._content["symbols"].items():
            mandatory_symbols[version_tag] = set(get_iter(symbol_obj, "all"))
            mandatory_symbols[version_tag] |= set(get_iter(symbol_obj, arch))

        for symbol_in_so in library.symbols():
            name = symbol_in_so.mangled_name()
            tag = symbol_in_so.symbol_version()
            if name in ignored_symbols:
                if tag != "":
                    print("Warning: Symbol %s is ignored but has version tag %s"
                          % (name, tag), file=sys.stderr)
                continue
            ignore_not_found = False
            if symbol_in_so.is_function() and symbol_in_so.is_weak():
                # ignore all weak functions per default
                ignore_not_found = True
            if tag not in mandatory_symbols:
                if not ignore_not_found:
                    symbols_not_in_yaml.append(symbol_in_so)
                continue
            if name not in mandatory_symbols[tag]:
                if not ignore_not_found:
                    symbols_not_in_yaml.append(symbol_in_so)
                continue
            mandatory_symbols[tag].remove(name)

        if len(symbols_not_in_yaml) != 0:
            ret = False
            outfile.write("unversioned symbols:\n")
            for symbol in symbols_not_in_yaml:
                outfile.write("  # %s %s %s\n  - %s\n" %(
                    symbol._type, symbol._bind, symbol.unmangled_name(), symbol.mangled_name()
                ))

        missing_symbols_printed = False
        for tag, missing_symbols in mandatory_symbols.items():
            if len(missing_symbols) == 0:
                continue
            ret = False
            if not missing_symbols_printed:
                outfile.write("missing symbols:\n")
                missing_symbols_printed = True
            for symbol in missing_symbols:
                outfile.write("  # %s\n  - %s\n" % (
                    cxxfilt(symbol), symbol
                ))
        return ret

    def generateDebianSymbolsFile(self, outfile: typing.IO, _sofile: str):
        library = SharedLibrary()
        library.readElfFile(_sofile)
        outfile.write("%s %s #MINVER#\n" % (library.soname(), library.debianPackageName()))
        # API information in version tag is equal to major.minor of deb package version
        for version_tag, symbols in self._content["symbols"].items():
            package_ver = version_tag.split("_")[-1]
            attribute="symver"
            if symbols.get("is_private", False):
                attribute = attribute + "|optional"
            outfile.write(" (%s)%s %s\n" % (attribute, version_tag, package_ver))
        # append ignored symbols as optional,
        # use "Base" as symbol version in case no version is set
        if "ignored_symbols" in self._content and self._content["ignored_symbols"] is not None:
            for symbol in self._content["ignored_symbols"]:
                outfile.write(" (optional)%s@Base 0.1\n" % symbol)
        for symbol in library.getWeakFunctions():
            outfile.write(" (optional)%s@%s 0.1\n"
                          % (symbol.mangled_name(), symbol.symbol_version_or("Base")))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog='symbol_version_tool'
    )
    parser.add_argument(
        'command',
        help='Command.',
        choices=[
            "GenerateDebianSymbolsFile",
            "GenerateLinkerVersionScript",
            "CheckGeneratedLibraries",
        ]
    )
    parser.add_argument(
        '-i',
        '--symbol_file',
        required=True,
        type=argparse.FileType('r'),
        help='Yaml input file with symbols'
    )
    parser.add_argument(
        '--arch',
        help='Select architecture-dependent symbols',
        choices=[
            '',
            'arm',
            'generic32',
            'generic64',
            'generic32-lto',
            'generic64-lto',
            'x86',
        ]
    )
    parser.add_argument(
        '-o',
        '--output',
        type=argparse.FileType('w'),
        help='Output file.',
        default='-',
    )
    parser.add_argument(
        '--library',
        help='Shared library elf file.',
    )
    args = parser.parse_args()
    s = SymbolMap()
    s.load(args.symbol_file)

    if args.command == "GenerateLinkerVersionScript":
        s.generateLinkerVersionScript(args.output, args.arch)
    elif args.command == "CheckGeneratedLibraries":
        if not s.checkGeneratedLibraries(args.arch, args.library, args.output):
            exit(1)
    elif args.command == "GenerateDebianSymbolsFile":
        s.generateDebianSymbolsFile(args.output, args.library)
